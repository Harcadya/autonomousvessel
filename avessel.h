/*=============================================================================

  Author:
              Andry Maykol Gomes Pinto, 2015

  Redistribution of this file, in original or modified form, without
  prior written consent of the autor(s) is prohibited.
-------------------------------------------------------------------------------

  File:        AVessel.h

  Description: Agregates modules

  Note:        Qt and OpenCV

  Files:      see includes
-------------------------------------------------------------------------------

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#ifndef AVessel_H
#define AVessel_H

//QT
#include <QObject>
#include <QMutex>
#include <QTimer>

//Boost
#include <boost/shared_ptr.hpp>

//OpenCV
#include <opencv2/opencv.hpp>

//C&C++
#include <time.h>
#include <string>

// Modules
#include "MonoCalib.h"



enum _ModeAVessel { AVessel_INTRINSIC = 0, AVessel_EXTRINSIC, AVessel_RUNNING};

//! Structure for saving the configuration for the program.s
struct _configProgram
{
    cv::Size calib_board_size;           //!< # corners.
    float calib_board_distance;          //!< distance between consecutive corners  in [m].
    int calib_numb_frames;               //!< number of frames used during the intrinsic calibration.
    std::string filename_intrinsic;      //!< directory where the intrinsic configuration exists.
    std::string filename_extrinsic;    //!< directory where the extrinsic configuration exists.

    int idCam;                           //!< Index of the camera to grab frames.
    float fps;                           //!< Framerate of the main program (acquisition).
    float scaleImage;                    //!< Ratio of the initial image (for processing).
    _ModeAVessel mode;                   //!< Starting mode { AVessel_INTRINSIC = 0, AVessel_EXTRINSIC, AVessel_RUNNING}.


    //! Segmentation valiables.
    cv::Scalar seg_colorMean;                        //!< color mean for the segmentation.
    cv::Scalar seg_colorStd;                         //!< standard deviation for the segmentation.
};

/**
 *
 * @brief Class that handles with VisionProgram + TimeoutMechanism
 *
 */

class AVessel : public QObject
{
    Q_OBJECT
private:
    boost::shared_ptr<cv::VideoCapture> videoCapture; //!< Video.

    QTimer * timer_cicle;                             //!< Timer that gives the main cicle - framerate according to "_fps".
    _configProgram config;
    cv::Mat img;                                      //!< Current image.


    void segmentation_Color(const cv::Mat &src, cv::Mat &dst);
    void segmentation_Channel(cv::Mat &src, cv::Mat &dst, int thr_min, int thr_max);
    void extractCentroid(const cv::Mat & src_seg , std::vector<cv::Point2d> &arr_centroids);

    // Debug
    int _step;

public:
    //! Constructor.
    AVessel(QObject *parent = 0);
    //! Destructor.
    ~AVessel();

    /// Modules elements.
    boost::shared_ptr<MonoCalib> calibrator;          //!< module for calibration.

    //! Loading file.
    bool loadConfigurationProgram(const std::string &filename, _configProgram &pconfig);


    std::vector<cv::Point2d> arr_centroids;          //!< Array of centroids for objects detected by segmentation [Pixels]
    std::vector<cv::Point3d> arr_centroids_World;    //!< Array of centroids for objects detected by segmentation [m].

public slots:
    /**
     * @brief SLOT: slot_mainCicle is active when a timer_cicle triggers
     */
    void slot_mainCicle(void);


};

#endif // AVessel_H
