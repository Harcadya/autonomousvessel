/*
    Authors: Andry Maykol Pinto.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
    STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
    WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <opencv2/opencv.hpp>
#include <cmath>
#include <iostream>
#include <ctime>



//! UTILITY FUNCTIONS:
/*!
    A set of utility functions
*/
namespace UTILS
{
    //! normalize the angle between [-PI, PI]
    static double normalizeAngle(double angle);
    //! Estimate the depth considering the minor and max axis in pixels.
    static float depthEstimation(float realAxisMajor, float AxisMinor, cv::Mat &camera_matrix, float min_axis_pixel, float max_axis_pixel);
    //! Print the Histogram
    static void printHistogram(const cv::Mat &hist, int binNumber_mag, cv::Mat &img);
    //! Compute dynamic Histogram (and print if is wanted)
    static void calc_DynHistogram(const cv::Mat &data, int &binNumber, cv::Mat &hist, bool printHist, char *windName, double rangeMax, double rangeMin );
    //! Convert the histogram in two vectors:: BinValues and BinPositions (using the range);
    static void vector_Histogram(const cv::Mat &Hist, float range[2], std::vector<int> &binValue, std::vector<float> &binPosition );
    //! Compute derivatives of the binValues
    static void derivativeBins(std::vector<float> &binValue, std::vector<float> &deriv, bool isAngle);
    static void derivativeBins(const std::vector<int> &binValue, std::vector<int> &deriv);
    //! See what the bins that are local maximums (return the descending indexes for the maximums).
    static void find_localmax_Bins(const std::vector<int> &binValue, std::vector<int> &maxIdx, float scale);
    //! Remove small maximums from the histogram vector: remove maximums with lower binValue than totalData*ratio.
    static void remove_smallMax_Bins(const std::vector<int> &binValue, std::vector<int> &maxIdx, float ratio);

    //! Uniform random value.
    static double unifRand();
    static double unifRand_d(double a, double b);
    static int unifRand(double a, double b);
    //! Estimate the Plane Equation Using 3 points.
    static void calc_planeEquation(cv::Point3f p1, cv::Point3f p2, cv::Point3f p3, float &a, float &b, float &c, float &d);
    //! Calc the value of Z coordinate in the plane.
    static float Zval_planeEquation(cv::Point2f p, float a, float b, float c, float d);
    //! Apply transformation to pointIn using transformation of affineT.
    static void applyAffine(cv::Point2d pointIn,cv::Point2d &pointOut , cv::Mat affineT);


    //! Convert 0-2046 to meters.
    static float depth16_to_meters(int raw_depth);
    static float depth16_to_meters2(int raw_depth);
    //! Convert the Depth matrix, 16bits, to a distance matrix (float).
    static void convert_Mat_depth16_to_meters(cv::Mat &src_16, cv::Mat &dst_f);
    static void convert_Mat_depth16_to_meters2(cv::Mat &src_16, cv::Mat &dst_f);
    //! Axis convertions:
    //! Convert imagePixel to imagePlane (focal distance)
    static void pixel2imgPlane(cv::Point2d pixel,cv::Point2d &imgPlane, cv::Mat cameraMatrix);
    static void imgPlane2pixel(cv::Point2d imgPlane, cv::Point2d &pixel, cv::Mat cameraMatrix);
    //! Convert imagePlane (focal distance) to a plane at Z_scale from the device
    static void imgPlane2wDevicePlane(cv::Point2d &imgPlane, double Z_scale);
    static void wDevicePlane2imgPlane(cv::Point2d &imgPlane, double Z_scale);
    static void pixel2world(cv::Point2d imgPoint,cv::Point3d &worldPoint, cv::Mat cameraMatrix, cv::Mat rotationMatrix,cv::Mat translationVector, double Z_scale);
    static void world2pixel(cv::Point3d worldPoint, cv::Point2d &imgPoint, cv::Mat cameraMatrix, cv::Mat rotationMatrix,cv::Mat translationVector);

    //! Convert world to Navigation
    static void world2Navigation(cv::Point3d worldPoint, cv::Point3d &navPoint, cv::Mat rotationMatrix,cv::Mat translationVector);


    //! LSQ Plane.
    //! Estimate the Plane Equation Using cv::Mat with Mask.
    static void calc_planeEquation(cv::Mat &src, cv::Mat mask, float &a, float &b, float &c, float &d);


}






/// **************************************************************************************************************************************
/// **************************************************************************************************************************************
/// UTILS

static double UTILS::normalizeAngle(double angle)
{
    if ((angle <= M_PI) && (angle >= -M_PI))
       return angle;
    else
    if (angle > M_PI)
        return (angle - 6.28318);
    else
    if (angle < -M_PI)
        return (angle + 6.28318);
    //return std::atan2(std::sin(angle), std::cos(angle));   //it takes much time
}

static void UTILS::printHistogram(const cv::Mat &hist, int binNumber_mag, cv::Mat &img)
{
    int hist_w = 500;
    int hist_h = 300;
    int bin_w  = cvRound( (double) hist_w / (double) binNumber_mag );
    img = cv::Mat ( hist_h, hist_w, CV_8UC3, cv::Scalar( 255,255,255) );

    cv::Scalar sumVal = cv::sum(hist);  //sum of all elements
    //max bin
    double maxVal = 0;
    cv::minMaxLoc(hist, 0, &maxVal, 0, 0);
    //    maxVal/=255;


    for(int h = 0; h < binNumber_mag; h++ )
    {
        float binVal  = hist.at<float>(h);
        int intensity = cvRound(binVal*255/ maxVal);
        cv::rectangle( img, cv::Point(h*bin_w, hist_h),
                       cv::Point( (h+1)*bin_w - 1, hist_h - binVal/sumVal.val[0] * hist_h),
                       cv::Scalar(intensity, 0, 0),
                       cv::FILLED );

        cv::putText(img, ".", cv::Point((h + 0.5)*bin_w, hist_h),
                    cv::FONT_HERSHEY_COMPLEX_SMALL, 1, cv::Scalar(0,0,0), 1, 8);

//        char str[4];
//        sprintf(str,"%d", (int)(h+0.5)*bin_w);
//        cv::putText(img, str, cv::Point((h+0.5)*bin_w, hist_h-2-h),
//                    cv::FONT_HERSHEY_COMPLEX_SMALL, 1, cv::Scalar(255,0,0), 1, 8);
//        std::cout<<str<<std::endl;
//        std::cout<<cvRound((double)(h+0.5)*bin_w)<<std::endl;
    }
    return;
}


//! Estimate the depth of the Part (detected) using the property (real dimensions) of the class: axisMajor and axisMinor.
/*!
  \param axisMajor              : (input) real size (in [mm] of the major axis of the Part : lenght
  \param axisMinor              : (input) real size (in [mm] of the minor axis of the Part : width
  \param camera_matrix          : (input) matrix of the instrinsic parameters
  \param max_axis_pixel         : (input) size of the major axis  in pixels.
  \param min_axis_pixel         : (input) size of the minor axis  in pixels.
  \return depth                 : [m]
*/
float UTILS::depthEstimation(float axisMajor, float axisMinor, cv::Mat &camera_matrix, float min_axis_pixel, float max_axis_pixel)
{
    /// Estimate the DEPTH:
    float d1 = (axisMajor*camera_matrix.at<double>(0,0) + camera_matrix.at<double>(0,2)) / max_axis_pixel;
    float d2 = (axisMinor*camera_matrix.at<double>(1,1) + camera_matrix.at<double>(1,2)) / min_axis_pixel;
    return ((d1 + d2)/2) / 1000;
}




static void UTILS::calc_DynHistogram(const cv::Mat &data, int &binNumber, cv::Mat &hist, bool printHist, char *windName, double rangeMax=-INFINITY, double rangeMin=-INFINITY )
{
    // Setup
    bool uniform    = true;       // bins will have the same size
    bool accumulate = false;      // clear the histograms in the beginning

    /// ***
    /// Histogram:
    cv::Scalar mean_mag, std_mag;
    double max_value  = rangeMax,
           min_value  = rangeMin;


    if(rangeMin == -INFINITY)
    {
        cv::meanStdDev(data, mean_mag, std_mag);
        min_value = mean_mag.val[0] - 2*std_mag.val[0];
    }
    if(rangeMax == -INFINITY)
    {
        cv::meanStdDev(data, mean_mag, std_mag);
        max_value = mean_mag.val[0] + 2*std_mag.val[0]; //95.5% of data if gaussian distribution (approx.)
    }



    // value range
    float range[] = { min_value, max_value } ;       //the upper boundary is exclusive
    const float* histRange = { range };
    // histogram mat
    cv::calcHist(&data, 1, 0, cv::Mat(), hist, 1, &binNumber, &histRange, uniform, accumulate );


    // Draw Histogram:
    if (printHist)
    {
        //        cv::Mat hist_norm,
        //        cv::normalize(hist, hist_norm, 0, 1, cv::NORM_MINMAX, -1, cv::Mat() );
        cv::Mat histImage;
        UTILS::printHistogram(hist, binNumber, histImage);
        cv::imshow(windName, histImage);
    }


#ifdef debug
    std::cout<<"Min:"<<min_value<<"  Mean:"<<mean_mag.val[0]<<"  Max:"<<max_value<<std::endl;
    //Debug
    cv::Mat data_norm, data_8;
    cv::normalize(data, data_norm, 0, 255, cv::NORM_MINMAX, -1, cv::Mat() );
    data_norm.convertTo(data_8, CV_8UC1);
    cv::imshow("MAG", data_8);
#endif
        return;
}



static void UTILS::vector_Histogram(const cv::Mat &Hist, float range[2], std::vector<int> &binValue, std::vector<float> &binPosition )
{
    binValue.resize(Hist.rows);
    std::copy(Hist.begin<float>(), Hist.begin<float>()+Hist.rows, binValue.begin());

    binPosition.reserve(binValue.size());
    float d_Pos = (range[1]-range[0])/(Hist.rows);
    for(int i = 0; i <binValue.size(); ++i)
            binPosition.push_back(range[0] + i*d_Pos);
    return;
}

static void UTILS::derivativeBins(std::vector<float> &binValue, std::vector<float> &deriv, bool isAngle)
{
    deriv.reserve(binValue.size());
    for (int i = 1; i < binValue.size(); i++)
    {
        if (isAngle)
        {
            if ( i != 0)
                deriv.push_back((float) UTILS::normalizeAngle(binValue[i] - binValue[i-1]));
            else
                deriv.push_back((float) UTILS::normalizeAngle(binValue[0] - binValue[binValue.size()]));
        }
        else
        {
//            if ( i != 0)
//                deriv.push_back(binValue[i] - binValue[i-1]);
//            else
//                deriv.push_back(-(binValue[0] - binValue[1]));
            deriv.push_back(binValue[i] - binValue[i-1]);
        }
    }
    return;
}

static void UTILS::derivativeBins(const std::vector<int> &binValue, std::vector<int> &deriv)
{
    deriv.clear();
    for (int i = 0; i < binValue.size(); i++)
    {
        if( i == 0)
           deriv.push_back(binValue[i] - binValue[i+1]);
        else
            deriv.push_back(binValue[i] - binValue[i-1]);
    }
    return;
}

static void UTILS::find_localmax_Bins(const std::vector<int> &binValue, std::vector<int> &maxIdx, float scale = 1.20)
{
    bool max[binValue.size()];
    int n_max = 0;

    for (int i = 0; i < binValue.size(); i++)
    {
        //INIT BIN
        if (i == 0)
        {
            if ((float)binValue[i] > scale*(float)binValue[i+1])
            {
                n_max++;
                max[i] = true;
            }
            else
                max[i] = false;
        }
        //END BIN
        else if (i == binValue.size() - 1)
        {
            if (((float)binValue[i] > scale*(float)binValue[i-1]) && ((float)binValue[i] > scale*(float)binValue[i-2]))
            {
                n_max++;
                max[i] = true;
            }
            else
                max[i] = false;
        }
        else
        {
            bool isMax = (((float)binValue[i] > scale*(float)binValue[i+1]) && (((float)binValue[i] > (float)binValue[i-1])))
                        || (((float)binValue[i] > (float)binValue[i+1]) && (((float)binValue[i] > (float)scale*binValue[i-1])));
            if (isMax)
            {
                n_max++;
                max[i] = true;
            }
            else
                max[i] = false;
        }
    }

    maxIdx.reserve(n_max);
    // Search only for the maximums and save in the vector (descending order)
    for (int i = 0; i < n_max; i++)
    {
        int _max   = 0;
        int idxMax = 0;

        for (int j = 0; j < binValue.size(); j++)
        {
            /// LOCAL MAX
            if (max[j]==true)
            {
                // See if satisfy the minimum binvalue and is the max in this moment
                if (binValue[j] > _max)
                {
                    _max   = binValue[j];
                    idxMax = j;
                }
            }
        }
        //Save:
        maxIdx.push_back(idxMax);
        max[idxMax] = false;
    }

    return;
}


static void UTILS::remove_smallMax_Bins(const std::vector<int> &binValue, std::vector<int> &maxIdx, float ratio)
{
//    for (int i = 0; i<maxIdx.size(); i++)
//    {
//        std::cout<<"Max->"<<maxIdx[i]<<" = "<<binValue[maxIdx[i]]<<std::endl;
//    }

    int totalsum = 0;
    for (int i = 0; i < binValue.size(); i++)
        totalsum+=binValue[i];

    int thrValue = cvRound(ratio *totalsum);
//    std::cout<<"TTT"<<totalsum<<"  Threshold"<<thrValue<<std::endl;

    std::vector<int> to_delete;
    for (int i = 0; i < maxIdx.size(); i++)
    {
        //FLAG REMOVE
        if(binValue[maxIdx[i]] < thrValue)
            to_delete.push_back(i);
    }

    int count_deleted = 0;
    for (int i = 0; i < to_delete.size(); i++)
    {
        maxIdx.erase(maxIdx.begin()+(to_delete[i]-count_deleted));
        count_deleted++;
    }


#ifdef DEBUG_K
    for (int i = 0; i<maxIdx.size(); i++)
    {
        std::cout<<"EMax->"<<maxIdx[i]<<" Value= "<< binValue[maxIdx[i]]<<std::endl;
    }
#endif

    return;
}




double UTILS::unifRand()
{
    return rand() / double(RAND_MAX);
}

double UTILS::unifRand_d(double a, double b)
{
    return (b-a)*unifRand() + a;
}
int UTILS::unifRand(double a, double b)
{
    return cvRound((b-a)*unifRand() + a);
}



void UTILS::calc_planeEquation(cv::Point3f p1, cv::Point3f p2, cv::Point3f p3, float &a, float &b, float &c, float &d)
{
    cv::Point3f p12 = p2 - p1;
    cv::Point3f p13 = p3 - p1;
    cv::Point3f abc = p12.cross(p13);
    a = abc.x;   //    a = (p2.y - p1.y)*(p3.z - p1.z)-(p3.y-p1.y)*(p2.z - p1.z);
    b = abc.y;   //    b = (p2.z - p1.z)*(p3.x - p1.x)-(p3.z-p1.z)*(p2.x - p1.x);
    c = abc.z;   //    v = (p2.x - p1.x)*(p3.y - p1.y)-(p3.x-p1.x)*(p2.y - p1.y);
    d = -(a * p1.x + b * p1.y + c * p1.z);
    return;
}


float UTILS::Zval_planeEquation(cv::Point2f p, float a, float b, float c, float d)
{
   return  -(p.x * a + p.y * b  + d)/ c;
}



void UTILS::applyAffine(cv::Point2d pointIn,cv::Point2d &pointOut , cv::Mat affineT)
{
    cv::Mat pointInMat = cv::Mat(cv::Point3d(pointIn.x,pointIn.y,1));
    cv::Mat newPointMat = affineT*pointInMat;
    pointOut = cv::Point2d(newPointMat.at<double>(0,0),newPointMat.at<double>(1,0));
    return;
}



float UTILS::depth16_to_meters(int raw_depth)
{
    if (raw_depth < 2046)
        return (1.0 / (raw_depth * -0.0030711016 + 3.3309495161) );
    else
        return 0;
}


float UTILS::depth16_to_meters2(int raw_depth)
{
    if (raw_depth < 2046)
    {
        return (raw_depth*raw_depth*raw_depth*2.0E-08) - 3E-05*raw_depth*raw_depth + 0.0149 * raw_depth - 2.4271;
    }
    return 0;
}

//Better approximation is : distance = 0.1236 * tan(rawDisparity / 2842.5 + 1.1863) [m]
void UTILS::convert_Mat_depth16_to_meters2(cv::Mat &src_16, cv::Mat &dst_f)
{

    dst_f = cv::Mat(src_16.rows, src_16.cols, CV_32FC1, cv::Scalar::all(0.0));
    cv::Mat aux;
    src_16.convertTo(aux, CV_32FC1);

    cv::Mat aux_2 = aux.mul(aux);

    dst_f = (aux.mul(aux_2) * 2E-08 ) - (3E-05 * aux_2)  + (0.0149 * aux) - 2.4271;

    //Search for strange values (deadzones) and put it to zero:
    for(int irow = 0; irow < dst_f.rows; irow++)
    {
        float * data_dist   = dst_f.ptr<float>(irow);
        for(int icol = 0; icol < dst_f.cols; icol++)
        {
            if(data_dist[icol] < 0)
                data_dist[icol] = 0;
        }
    }
    return;
}




////Better approximation is : distance = 0.1236 * tan(rawDisparity / 2842.5 + 1.1863) [m]
//void UTILS::convert_Mat_depth16_to_meters(cv::Mat &src_16, cv::Mat &dst_f)
//{
//    dst_f = cv::Mat(src_16.rows, src_16.cols, CV_32FC1, cv::Scalar::all(0.0));
//    src_16.convertTo(dst_f, CV_32FC1);
//    dst_f = (dst_f * -0.0030711016) + 3.3309495161;
//    dst_f = 1.0 / dst_f;


//    //Search for strange values (deadzones) and put it to zero:
//    for(int irow = 0; irow < dst_f.rows; irow++)
//    {
//        float * data_dist   = dst_f.ptr<float>(irow);
//        for(int icol = 0; icol < dst_f.cols; icol++)
//        {
//            if(data_dist[icol] < 0)
//                data_dist[icol] = 0;
//        }
//    }
//    return;
//    //    return (1.0 / (raw_depth * -0.0030711016 + 3.3309495161) );
//}



// Function given by Eduardo.

void UTILS::convert_Mat_depth16_to_meters(cv::Mat &src_16, cv::Mat &dst_f)
{

    dst_f = cv::Mat(src_16.rows, src_16.cols, CV_32FC1, cv::Scalar::all(0.0));
    cv::Mat aux;
    src_16.convertTo(aux, CV_32FC1);

    cv::Mat aux_2 = aux.mul(aux);

    dst_f = (0.000007 * aux_2)  - (0.007047 * aux) + 2.3935;

    //Search for strange values (deadzones) and put it to zero:
    for(int irow = 0; irow < dst_f.rows; irow++)
    {
        float * data_dist   = dst_f.ptr<float>(irow);
        for(int icol = 0; icol < dst_f.cols; icol++)
        {
            if(data_dist[icol] < 0)
                data_dist[icol] = 0;
        }
    }
    return;
}




//! Convert imagePixel to imagePlane (focal distance)
void UTILS::pixel2imgPlane(cv::Point2d pixel,cv::Point2d &imgPlane, cv::Mat cameraMatrix)
{
    imgPlane.x = (pixel.x - cameraMatrix.at<double>(0,2))  / cameraMatrix.at<double>(0,0); //[No scale]
    imgPlane.y = (pixel.y - cameraMatrix.at<double>(1,2))  / cameraMatrix.at<double>(1,1); //[No scale]
    return;
}



//! Convert imagePlane (focal distance) to a plane at Z_scale from the device
void UTILS::imgPlane2wDevicePlane(cv::Point2d &imgPlane, double Z_scale)
{
    imgPlane = imgPlane*Z_scale; //[m]
    return;
}

//! Convert from a plane at Z_scale from the device to imagePlane (focal distance)
void UTILS::wDevicePlane2imgPlane(cv::Point2d &imgPlane, double Z_scale)
{
    imgPlane.x = imgPlane.x/(Z_scale); //[m]
    imgPlane.y = imgPlane.y/(Z_scale); //[m]
    return;
}

//! Convert imagePlane (focal distance) to imagePixel
void UTILS::imgPlane2pixel(cv::Point2d imgPlane, cv::Point2d &pixel, cv::Mat cameraMatrix)
{
    double x,y;
    x = imgPlane.x * cameraMatrix.at<double>(0,0) + cameraMatrix.at<double>(0,2); //[No scale]
    y = imgPlane.y * cameraMatrix.at<double>(1,1) + cameraMatrix.at<double>(1,2); //[No scale]

    pixel = cv::Point2d(x,y);
}




void UTILS::pixel2world(cv::Point2d imgPoint,cv::Point3d &worldPoint, cv::Mat cameraMatrix, cv::Mat rotationMatrix,cv::Mat translationVector, double Z_scale)
{
    cv::Point2d imgPlane;
    pixel2imgPlane(cv::Point2d(imgPoint.x,imgPoint.y),imgPlane, cameraMatrix);
    imgPlane2wDevicePlane(imgPlane, Z_scale);

    cv::Mat wp = rotationMatrix.inv()* (cv::Mat(cv::Point3d(imgPlane.x,imgPlane.y,Z_scale))-translationVector);
    worldPoint = cv::Point3d(wp.at<double>(0,0), wp.at<double>(1,0), wp.at<double>(2,0));
    return;
}



void UTILS::world2pixel(cv::Point3d worldPoint, cv::Point2d &imgPoint, cv::Mat cameraMatrix, cv::Mat rotationMatrix,cv::Mat translationVector)
{
    cv::Mat imgp = rotationMatrix * cv::Mat(worldPoint) + translationVector;
    cv::Point2d imgPlane = cv::Point2d(imgp.at<double>(0,0),imgp.at<double>(1,0));

    wDevicePlane2imgPlane(imgPlane, imgp.at<double>(2,0));
    imgPlane2pixel(imgPlane, imgPoint, cameraMatrix);
    return;
}


void UTILS::world2Navigation(cv::Point3d worldPoint, cv::Point3d &navPoint, cv::Mat rotationMatrix, cv::Mat translationVector)
{
    cv::Mat imgp = rotationMatrix * cv::Mat(worldPoint) + translationVector;

    //std::cout<<" ------->>>"<<imgp.at<double>(2,0) - worldPoint.z<<std::endl;
    navPoint     = cv::Point3d(imgp.at<double>(0,0),imgp.at<double>(1,0),imgp.at<double>(2,0)  );//
    return;
}





//! Planar Fitting of 3D Points of Form (x, y, f (x, y))
//! \note The error is measured only in the z-direction
//! \param src   : distance Mat in [m]
//! \param mask  : mask matrix for pixels that should be considered
void UTILS::calc_planeEquation(cv::Mat &src, cv::Mat mask, float &a, float &b, float &c, float &d)
{
    float sum_xx = 0.0;
    float sum_xy = 0.0;
    float sum_x  = 0.0;
    float sum_xz = 0.0;
    float sum_yy = 0.0;
    float sum_y  = 0.0;
    float sum_yz = 0.0;
    float sum_z  = 0.0;
    float n_elem = 0.0;

    cv::Point3f p1;

    for(int irow = 0; irow < src.rows; irow++)
    {
        float *data_src  = src.ptr<float>(irow);
        uchar *data_mask = mask.ptr<uchar>(irow);
        for(int icol = 0; icol < src.cols; icol++)
        {
            if( data_mask[icol] > 0 )
            {
                sum_xx += (float) icol * icol;
                sum_xy += (float) icol * irow;
                sum_x  += (float) icol;
                sum_xz += (float) icol * data_src[icol];
                sum_yy += (float) irow * irow;
                sum_y  += (float) irow;
                sum_yz += (float) irow * data_src[icol];
                sum_z  += data_src[icol];
                n_elem++;

                p1.x = icol;
                p1.y = irow;
                p1.z = data_src[icol];
            }
        }
    }

    cv::Mat mat_A(3,3, CV_32FC1, cv::Scalar::all(0.0));
    mat_A.at<float>(0,0) = sum_xx;
    mat_A.at<float>(0,1) = sum_xy;
    mat_A.at<float>(0,2) = sum_x;
    mat_A.at<float>(1,0) = sum_xy;
    mat_A.at<float>(1,1) = sum_yy;
    mat_A.at<float>(1,2) = sum_y;
    mat_A.at<float>(2,0) = sum_x;
    mat_A.at<float>(2,1) = sum_y;
    mat_A.at<float>(2,2) = n_elem;
    cv::Mat mat_b(3,1, CV_32FC1, cv::Scalar::all(0.0));
    mat_b.at<float>(0,0) = sum_xz;
    mat_b.at<float>(1,0) = sum_yz;
    mat_b.at<float>(2,0) = sum_z;
    cv::Mat mat_x(3,1, CV_32FC1, cv::Scalar::all(0.0));

    mat_x = mat_A.inv() * mat_b;
    a =  mat_x.at<float>(0,0);
    b =  mat_x.at<float>(1,0);
    c =  mat_x.at<float>(2,0);
    d = -(a * p1.x + b * p1.y + c * p1.z);
    return;
}
