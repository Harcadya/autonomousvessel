/*=============================================================================

  Author:
              Andry Maykol Gomes Pinto, 2015

  Redistribution of this file, in original or modified form, without
  prior written consent of the autor(s) is prohibited.
-------------------------------------------------------------------------------

  File:        MonoCalib.h

  Description: Agregates modules

  Note:        OpenCV

  Files:      see includes
-------------------------------------------------------------------------------

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#ifndef MONOCALIB_H
#define MONOCALIB_H

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//! State of the Calibration Procedure that should be performed.
enum _CalibState
{
    _CalibOFF = 0,
    _CalibSTART,
    _CalibINTRINSIC,
    _CalibEXTRINSIC,
};

//! Class used for callbacks
struct CallbackHelper
{
    cv::Mat K_cameraMatrix, Ext_Rot, Ext_Trans;
    cv::Mat rgb;
};



class MonoCalib
{
private:
    std::string directory; //!<directory of working;
    bool isready; //true if the initMaps of the intrinsic parameter is available

    //! State of the calibration that must be performed.
    _CalibState state_doCalib;
    int n_savedFrames;

    //! Current calibration of the system. See "hasIntrinsic() and hasExtrinsic()"
    bool isCalib_Intrinsic;
    bool isCalib_Extrinsic;

    //! Instrinsic calibration.
    int flag;                       //!< flag to specify how calibration is done.
    std::vector<std::string> filelist;
    std::vector< std::vector<cv::Point3f> > objectPoints;
    std::vector< std::vector<cv::Point2f> > imagePoints;
    //! Open images, detects chessboard and extracts corner points.
    int addChessboardPoints(const std::vector<std::string>& filelist, const cv::Size &boardSize, float boardSquareDist);
    //! Add object points and corresponding image points to the arrays that will be used in the intrinsic calibration.
    void addPoints(const std::vector<cv::Point2f>& imageCorners, const std::vector<cv::Point3f>& objectCorners);
    //! Sets the flag for the calibration.
    void setCalibrationFlag(bool radial8CoeffEnabled, bool tangentialParamEnabled);
    //! Calibrates the intrinsic parameters for the camera.
    double intrinsic_calibrate(cv::Size &imageSize);
    //! Calibrates the extrinsic parameters for the camera.
    bool calibExtrinsic(const cv::Mat &src, const cv::Size &boardSize, float boardSquareDist, std::vector<cv::Point2f> &imageCorners);


    //! Calback for pixel access for Extrinsic Calibration - depth calculation of each corner.
    CallbackHelper pp;
    static void mouseEvent(int evt, int x, int y, int flags, void* param) ;

public:
    cv::Mat mapx, mapy;
    cv::Mat K_cameraMatrix, LensDistortion_coef;
    cv::Mat Ext_Rot, Ext_Trans;     //!< Extrinsic calibration.
    cv::Size size;

    MonoCalib();
    MonoCalib(cv::Size imgsize);
    ~MonoCalib();

    //VideoCapture: return true if success
    bool openVideoCapture(int idxCapture, char * filenameCapture);
    //VideoWriter:  return true if success
    bool openVideoWriter(char * filenameWriter, int fps, cv::Rect rect);

    //! Load IntrinsicCalibration.
    bool loadConfiguration_INTRINSIC(std::string dir);
    //! Load ExtrinsicCalibration
    bool loadConfiguration_EXTRINSIC(std::string dir);
    //! Create the MapX and MapY for the undistortion process.
    void initMaps();
    //! Undistort Image.
    bool undistort(cv::Mat &src, cv::Mat &dst);   //return true if undistortion was possible
    //! Start the calibration process. First, it calibrates the intrinsic parameters and then the extrinsic.
    bool process_calibration_camera(const cv::Mat &src, const cv::Size &boardSize, float boardSquareDist, int maxFrames, std::vector<cv::Point2f> &imageCorners);

    //! Set the type of calibration to Intrinsic.
    void setCalibrationType(_CalibState mode);

    ///Video Grabber
    cv::VideoCapture *in_capture;
    ///Video Writer
    cv::VideoWriter *out_capture;

    //! Sets the directory of working (save and loading files).
    void setDirectory(std::string dir);

    //! Flags that are "true" when the parameter is available.
    bool hasIntrinsic();
    bool hasExtrinsic();

};



#endif // MONOCALIB_H
