/*
    Authors: Andry Maykol Pinto.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
    STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
    WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "MonoCalib.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <string.h>
#include <iostream>

#include "m_utils.h"

#define CODEC cv::VideoWriter::fourcc('D', 'I', 'V', 'X')


//DEFAULT PATH
#define DIR std::string("/home/andrypinto/Desktop/VeleiroAutonomo/Images/")

MonoCalib::MonoCalib()
{
    isready       = false; // mapx and mapy are not set
    state_doCalib = _CalibOFF;
    in_capture    = new cv::VideoCapture ();
    out_capture   = new cv::VideoWriter();
    n_savedFrames = 0;
    size.width    = 640;  //standard resolution
    size.height   = 480;  //standard resolution

    directory = DIR;
    //cv::namedWindow("Live", CV_WINDOW_NORMAL);


    isCalib_Intrinsic    = false;
    isCalib_Extrinsic    = false;
}

MonoCalib::MonoCalib(cv::Size imgsize)
{
    size.width    = imgsize.width;
    size.height   = imgsize.height;
    isready       = false; // mapx and mapy are not set
    state_doCalib = _CalibOFF;
    in_capture    = new cv::VideoCapture();
    out_capture   = new cv::VideoWriter();
    n_savedFrames = 0;

    cv::startWindowThread();
    //cv::namedWindow("Live", CV_WINDOW_NORMAL);

    directory = DIR;

    isCalib_Intrinsic    = false;
    isCalib_Extrinsic    = false;
}

MonoCalib::~MonoCalib()
{
    cv::destroyAllWindows();
    in_capture->release();
    out_capture->release();
    delete in_capture, out_capture;
}

/*!
 * \brief Opens the input device or file
 * \param idxCapture of the device
 * \param filenameCapture namefile or address of the device to be openned.
 * \return true if the output is ready.
 */
bool MonoCalib::openVideoCapture(int idxCapture, char *filenameCapture)
{
    if (idxCapture >-1)
        in_capture->open(idxCapture);
    else
        in_capture->open(filenameCapture);

    return in_capture->isOpened();
}

/*!
 * \brief Initialized the maps used to rectify image
 * \param filenameWriter filename of the video
 * \param fps is used to define the framerate
 * \param rect is used to define the size of the canvas.
 * \return true if the output is ready.
 */
bool MonoCalib::openVideoWriter(char *filenameWriter, int fps, cv::Rect rect)
{
    out_capture->open(filenameWriter, CODEC, fps, cv::Size(rect.width, rect.height), true);
    return out_capture->isOpened();
}

/*!
 * \brief Initialize the maps used to rectify image
 */
void MonoCalib::initMaps()
{
    try
    {
        cv::initUndistortRectifyMap(
                    K_cameraMatrix,  // computed camera matrix
                    LensDistortion_coef,    // computed distortion matrix
                    cv::Mat(),     // optional rectification (none)
                    cv::Mat(),     // camera matrix to generate undistorted
                    size,          //            image.size(),  // size of undistorted
                    CV_32FC1,      // type of output map
                    mapx, mapy);   // the x and y mapping functions
        isready = true;
    }
    catch(...)
    {
        std::cerr<<"MonoCalib::initMaps() :: failed"<<std::endl;
    }
    return;
}


/*!
 * \brief Undistort Image due to lens distortion.
 * \param src image distorted.
 * \param dst image undistorted.
 * \param "true" if success.
 */
bool MonoCalib::undistort(cv::Mat &src, cv::Mat &dst)
{
    if (isready)
        cv::remap(src, dst, mapx, mapy, cv::INTER_LINEAR); // interpolation type
    else
        dst = src;
    return isready;
}


/*!
 * \brief Imports the INTRINSIC Camera parameters.
 * \param dir is the path+filename
 * \return "true" if success
 *
 * \note It computed the MAPS for the rectification if the loading was successfull
 */
bool MonoCalib::loadConfiguration_INTRINSIC(std::string dir)
{
    cv::FileStorage fs_KD(dir, cv::FileStorage::READ);
    if (fs_KD.isOpened())
    {
        fs_KD ["ImgSize"] >> size;
        fs_KD ["Intrinsics"] >> K_cameraMatrix;
        fs_KD ["Distortion"] >> LensDistortion_coef;   // usually >>> 8 Coef: [k1, k2, p1, p2, k3, k4, k5, k6]...
        fs_KD.release();

        //! Calc the MAPS for the rectification.
        initMaps();

        isCalib_Intrinsic  = true;
        return true;
    }
    else
        return false;
}


/*!
 * \brief Imports the EXTRINSIC Camera parameters.
 * \param dir is the path+filename
 * \return "true" if success
 *
 * \note It computed the MAPS for the rectification if the loading was successfull
 */
bool MonoCalib::loadConfiguration_EXTRINSIC(std::string dir)
{
    cv::FileStorage fs_KD(dir, cv::FileStorage::READ);
    if (fs_KD.isOpened())
    {
        fs_KD ["Extrinsics_Rot"]   >> Ext_Rot;
        fs_KD ["Extrinsics_Trans"] >> Ext_Trans;
        fs_KD.release();

        isCalib_Extrinsic  = true;
        return true;
    }
    else
        return false;
}





/*!
 * \brief Open images, detects chessboard and extracts corner points
 * \param filelist array of filenames that will be used to import images (considered for the intrinsic calibration process)
 * \param boardSize is the number of corners H and W.
 * \param boardSquareDist is the metric size between two consecutive corners.
 * \return number of images with sucess in the detection of the chessboard
 */
int MonoCalib::addChessboardPoints(const std::vector<std::string>& filelist, const cv::Size &boardSize, float boardSquareDist)
{

    // the points on the chessboard
    std::vector<cv::Point2f> imageCorners;
    std::vector<cv::Point3f> objectCorners;


    int numSquares = boardSize.width * boardSize.height;
    // 3D Scene Points:
    for(int j=0;j<numSquares;j++)
        objectCorners.push_back(cv::Point3f(boardSquareDist*(j/boardSize.width), boardSquareDist*(j%boardSize.width), 0.0f));

    //    for (int i=0; i<boardSize.height; i++) {
    //        for (int j=0; j<boardSize.width; j++) {

    //            objectCorners.push_back(cv::Point3f((j*CALIB_SQUARE_DIST, i*CALIB_SQUARE_DIST, 0.0f));
    //        }
    //    }



    // 2D Image points:
    cv::Mat image; // to contain chessboard image
    int successes = 0;
    // for all viewpoints
    cv::namedWindow("Corners on Chessboard", cv::WINDOW_AUTOSIZE);
    for (int i = 0; i < filelist.size(); i++)
    {
        std::cout<<"Loading Images ...  ->"<<filelist[i]<<std::endl;
        // Open the image
        image = cv::imread(filelist[i], 0);

        // Get the chessboard corners
        bool found = cv::findChessboardCorners(image, boardSize, imageCorners);



        if(found)
        {
            // Get subpixel accuracy on the corners
            cv::cornerSubPix(image, imageCorners,
                             cv::Size(5,5),
                             cv::Size(-1,-1),
                             cv::TermCriteria(cv::TermCriteria::MAX_ITER +
                                              cv::TermCriteria::EPS,
                                              30,		// max number of iterations
                                              0.1));     // min accuracy

            // Add image and scene points from one view
            addPoints(imageCorners, objectCorners);
            successes++;

            //Color version
            image = cv::imread(filelist[i], 1);

            //Draw the corners
            cv::drawChessboardCorners(image, boardSize, imageCorners, found);
            cv::imshow("Corners on Chessboard", image);
        }
        cv::waitKey(100);
    }

    return successes;
}



/*!
 * \brief Add object points and corresponding image points to the arrays that will be used in the intrinsic calibration.
 * \param imageCorners array of 2D points that depict the image coordinates
 * \param objectCorners array of 3D points that depict the object
 */
void MonoCalib::addPoints(const std::vector<cv::Point2f>& imageCorners, const std::vector<cv::Point3f>& objectCorners)
{

    // 2D image points from one view
    imagePoints.push_back(imageCorners);
    // corresponding 3D scene points
    objectPoints.push_back(objectCorners);
    return;
}


/*!
 * \brief Sets the flag used in cv::calibrateCamera()
 * \param 8radialCoeffEnabled should be true if 8 radial coefficients are required (5 is default)
 * \param tangentialParamEnabled should be true if tangeantial distortion is present
 */
void MonoCalib::setCalibrationFlag(bool radial8CoeffEnabled, bool tangentialParamEnabled)
{
    flag = 0;
    if (!tangentialParamEnabled) flag += CV_CALIB_ZERO_TANGENT_DIST;
    if (radial8CoeffEnabled)     flag += CV_CALIB_RATIONAL_MODEL;
    return;
}



/*!
 * \brief Calibrates the intrinsic parameters for the camera.
 * \param imageSize is the image size.
 * \return the re-projection error.
 */
double MonoCalib::intrinsic_calibrate(cv::Size &imageSize)
{
    //Output rotations and translations
    std::vector<cv::Mat> rvecs, tvecs;
    double result;
    // start calibration
    result = cv::calibrateCamera(objectPoints, // the 3D points
                                 imagePoints,  // the image points
                                 imageSize,    // image size
                                 K_cameraMatrix, // output camera matrix
                                 LensDistortion_coef,   // output distortion matrix
                                 rvecs, tvecs, // Rs, Ts
                                 flag);
    /* XML SAVING */
    cv::FileStorage fs_KD(directory + "/IntrinsicsDistortion.xml", cv::FileStorage::WRITE);
    if (fs_KD.isOpened())
    {
        fs_KD << "ImgSize" << size;
        fs_KD << "Intrinsics" << K_cameraMatrix;
        fs_KD << "Distortion" << LensDistortion_coef;
        fs_KD.release();
    }
    isCalib_Intrinsic = true;
    return result;
}




/**
 * @brief MonoCalib::calibExtrinsic. Performs extrinsic calibration
 *
 * @note saves the Rotation and Translation matrices in "Ext_Rot" and "Ext_Trans"
 * It only performs when the Intrinsic parameters are available.
 *
 * @param src is the input image CV_8UC1, CV_8UC3.
 * @param boardSize is the number of corners in the chessboard
 * @param boardSquareDist is the size in metric units
 * @param imageCorners is the array of the points on the chessboard in pixel coordinates
 * @return "true" if success
 */
bool MonoCalib::calibExtrinsic(const cv::Mat &src, const cv::Size &boardSize, float boardSquareDist, std::vector<cv::Point2f> &imageCorners)
{
    try
    {
        /// ****************************************************
        /// Prepare image
        //Undistort if possible
        cv::Mat img_undistorted;
        cv::Mat ncons_src = src;               //due to the "const assignment"
        this->undistort(ncons_src, img_undistorted); //if not possible it returns the "ncons_src"

        cv::Mat rvect;
        imageCorners.clear();

        cv::Mat img_src;
        if(src.channels() > 1)
            cv::cvtColor(img_undistorted, img_src, CV_RGB2GRAY);
        else
            img_src = img_undistorted.clone();

        /// ****************************************************




        /// ****************************************************
        /// Extract corners
        // Get the chessboard corners
        bool foundCHESS = cv::findChessboardCorners(img_src, boardSize, imageCorners);

        if(foundCHESS)
        {

            // Get subpixel accuracy on the corners
            cv::cornerSubPix(img_src, imageCorners,
                             cv::Size(5,5),
                             cv::Size(-1,-1),
                             cv::TermCriteria(cv::TermCriteria::MAX_ITER +
                                              cv::TermCriteria::EPS,
                                              30,		// max number of iterations
                                              0.1));     // min accuracy

            std::vector<cv::Point3f> objectCorners;

            //Fill 3D Object.
            // 3D Scene Points:
            for(int ji = 0; ji<boardSize.height; ji++)
            {
                for(int jj = 0; jj<boardSize.width; jj++)
                    objectCorners.push_back(cv::Point3f(float(boardSquareDist * jj),
                                                        float(boardSquareDist * ji),
                                                        float(0.f)));
            }
            /// ****************************************************




            /// ****************************************************
            /// CALIBRATE
            // start calibration
            double result = cv::solvePnP(objectCorners,                        // the 3D points
                                         imageCorners,                         // the image points
                                         K_cameraMatrix,    // output camera matrix
                                         cv::Mat(),         // output distortion matrix
                                         rvect, Ext_Trans); // Rs, Ts
            cv::Rodrigues(rvect, Ext_Rot);



            /* XML SAVING */
            cv::FileStorage fs_KD_(directory + "/ExtrinsicCalib.xml", cv::FileStorage::WRITE);
            if (fs_KD_.isOpened())
            {
                fs_KD_ << "Extrinsics_Rot"   << Ext_Rot;
                fs_KD_ << "Extrinsics_Trans" << Ext_Trans;
                fs_KD_.release();
            }


            // Draw the corners.
            cv::Mat img_draw = img_undistorted;
            cv::drawChessboardCorners( img_draw, boardSize, cv::Mat(imageCorners), true );

            pp.Ext_Rot        = Ext_Rot.clone();
            pp.Ext_Trans      = Ext_Trans.clone();
            pp.K_cameraMatrix = K_cameraMatrix.clone();
            pp.rgb            = img_draw.clone();


            cv::namedWindow("Calib RGB");
            cv::setMouseCallback("Calib RGB", MonoCalib::mouseEvent, (void*) &pp);
            cv::putText(img_draw, "Extrinsic Image", cv::Point2i(20, 40), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0,0,255), 3);



            cv::imshow("Calib RGB", img_draw);
            isCalib_Extrinsic = true;
            cv::waitKey();

            /// ****************************************************
            return true;
        }
        else
            return false;
    }
    catch (std::string e)
    {
        std::cerr<<e<<std::endl;
    }
    catch (...)
    {
        std::cerr<<" MonoCalib::calibExtrinsic :: has failed"<<std::endl;
    }
}




/*!
 * \brief MonoCalib::mouseEvent. Callback for handing with mouse event.
 *
 */
void MonoCalib::mouseEvent(int evt, int x, int y, int flags, void *param)
{
    if (evt == CV_EVENT_LBUTTONDOWN)
    {
        CallbackHelper p       = *(CallbackHelper *)param;
        //std::cout<<" CALL:  "<<p.K_cameraMatrix<<"   "<<p.Ext_Rot<<"     "<<p.Ext_Trans<<std::endl;


        printf("%d %d: %d, %d, %d\n",
               x, y,
               (int) p.rgb.at<cv::Vec3b>(y, x)[0],
                (int) p.rgb.at<cv::Vec3b>(y, x)[1],
                (int) p.rgb.at<cv::Vec3b>(y, x)[2]);

        cv::Point2d _pixel;
        _pixel.x = (double) x;
        _pixel.y = (double) y;

        cv::Point3d wpixel;
        UTILS::pixel2world(_pixel, wpixel, p.K_cameraMatrix, p.Ext_Rot, p.Ext_Trans, p.Ext_Trans.at<double>(0,2));

        std::cout<<" wpixel ->"<<wpixel<<std::endl;
        //        cv::namedWindow("OnClick - Debug");
        //        cv::imshow("OnClick - Debug", p.rgb);

        cv::Mat uyv_src;
        cv::cvtColor(p.rgb, uyv_src, CV_BGR2YCrCb);
        std::cout<<" TYPE---->"<<p.rgb.type()<<"    "<<uyv_src.type()<<std::endl;
//        cv::imshow("YUV", uyv_src);
//        std::vector<cv::Mat> arr_yuv;
//        cv::split(uyv_src, arr_yuv);
//        cv::imshow("Y", arr_yuv[0]);
//        cv::imshow("U", arr_yuv[1]);
//        cv::imshow("V", arr_yuv[2]);

        printf("YCrCb %d %d: %d, %d, %d\n",
               x, y,
               (int) uyv_src.at<cv::Vec3b>(y, x)[0],
                (int) uyv_src.at<cv::Vec3b>(y, x)[1],
                (int) uyv_src.at<cv::Vec3b>(y, x)[2]);
    }

    return;
}

/**
 * @brief MonoCalib::setCalibrationType. Defines the type of the calibration procedure.
 * @param mode {_CalibINTRINSIC; _CalibEXTRINSIC == Extrinsic;}
 */
void MonoCalib::setCalibrationType(_CalibState mode)
{
    state_doCalib = mode;
    return;
}


/*!
 * \brief Start the calibration process. First, it calibrates the intrinsic parameters and then the extrinsic.
 * 1) Images are saved in the disk
 * 2) Intrinsic calibration
 * 3) Extrinsic calibration
 *
 * \param src is a input image, CV_8UC1 or CV_8UC3
 * \param maxFrames is the number of frames acquired for the Instrinsic calibration.
 * \param boardSize is the board size (in corners number)
 * \param boardSquareDist is the metric size between two consecutive corners.
 * \param imageCorners is the corners of the chessboard when extrinsic calibration is performed
 *
 * \return "true" then some calibration was conducted.
 *
 * \note 8radialCoeffEnabled should be true if 8 radial coefficients are required (5 is default)
 * \note tangentialParamEnabled should be true if tangential distortion is present
 */
bool MonoCalib::process_calibration_camera(const cv::Mat &src, const cv::Size &boardSize, float boardSquareDist, int maxFrames, std::vector<cv::Point2f> &imageCorners)
{
    try
    {
        std::string file_rg;
        switch (state_doCalib)
        {
        case _CalibINTRINSIC:  ///Intrinsic Calibration
        {
            if(n_savedFrames < maxFrames)
            {
                file_rg = directory + "rgb_"+ std::to_string(n_savedFrames) + ".png";
                std::cout<<"Saving Image ...  ->"<<file_rg<<std::endl;
                filelist.push_back(file_rg);
                cv::imwrite(file_rg, src);
                n_savedFrames++;


                cv::Mat src_draw = src.clone();
                cv::putText(src_draw, "Saving Image", cv::Point2i(20, 40), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0,0,255), 3);
                cv::namedWindow("Live");
                cv::imshow("Live", src_draw);
                cv::waitKey(1000);
                return false;
            }
            else
            {
                //prepare the data.
                addChessboardPoints(filelist, boardSize, boardSquareDist);
                setCalibrationFlag(false,true);
                cv::Size _size;
                _size.height  = src.rows;
                _size.width   = src.cols;
                //Calibrate.
                intrinsic_calibrate(_size);

                n_savedFrames = 0;
                std::cout<<" ProcessCali:: InitMaps"<<std::endl;

                // Recalculates the MAPS for Rectification.
                initMaps();     /// Create Maps for the rectification of the image.
                std::cout<<" ProcessCali:: InitMaps DONE!"<<std::endl;
                state_doCalib = _CalibOFF;
                return true;
            }
        }
            break;

        case _CalibEXTRINSIC:  ///Extrinsic Calibration
        {
            file_rg = directory + "/rgb_ext.png";
            cv::imwrite(file_rg, src);

            calibExtrinsic(src, boardSize, boardSquareDist, imageCorners);
            state_doCalib = _CalibOFF;
            return true;
        }
            break;

        default: /// STOP
            n_savedFrames = 0;
            state_doCalib = _CalibOFF;
            filelist.clear();
            return false;
            break;
        }
    }
    catch(...)
    {
        std::cerr<<" MonoCalib::process_calibration_camera ::: failed"<<std::endl;
    }

    return true;
}


/**
 * @brief MonoCalib::setDirectory
 * @param dir is the path
 */
void MonoCalib::setDirectory(std::string dir)
{
    directory = dir;
    return;
}

/**
 * @brief MonoCalib::hasIntrinsic
 * @return "true" if the Intrinsic calibration is available
 */
bool MonoCalib::hasIntrinsic()
{
    return isCalib_Intrinsic;
}

/**
 * @brief MonoCalib::hasExtrinsic
 * @return "true" if the Extrinsic calibration is available
 */
bool MonoCalib::hasExtrinsic()
{
    return isCalib_Extrinsic;
}

