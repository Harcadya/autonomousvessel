#include "avessel.h"
#include "m_utils.h"  // pixel to navigation frame

#define IDCAM 0
#define DIR_CONFIG "AVessel.conf"

AVessel::AVessel(QObject *parent):QObject(parent)
{
    _step = 0;
    std::cout<<" AVessel will wait 5s...."<<std::endl;
    //sleep(5);
    std::cout<<" AVessel is starting...."<<std::endl;

    /// LOAD CONFIGURATION FILE
    if (!loadConfigurationProgram(DIR_CONFIG, config))
    {
        config.calib_board_size     = cv::Size(9,6);
        config.calib_board_distance = 0.025;
        config.calib_numb_frames    = 25;
        config.idCam                = 0;
        config.fps                  = 5.0;
        config.scaleImage           = 1.0;
        config.mode                 = AVessel_RUNNING;
        config.seg_colorMean = cv::Scalar(90, 210, 80);   // RED
        config.seg_colorStd  = cv::Scalar(50, 30, 50);
    }


    /// VIDEOCAPUTE
    videoCapture.reset(new cv::VideoCapture(config.idCam));
    //videoCapture.reset(new cv::VideoCapture(std::string("/home/andrypinto/Desktop/VeleiroAutonomo/ConfigExamples/165308.mp4")));
    cv::Size _size(1280, 720);
    if( videoCapture->isOpened())
    {
        std::cout<<" AVessel :: camera is ready!"<<std::endl;
        _size.width  = videoCapture->get(CV_CAP_PROP_FRAME_WIDTH);
        _size.height = videoCapture->get(CV_CAP_PROP_FRAME_HEIGHT);
        if( ( _size.width > 0 ) && (_size.height > 0) )
            img = cv::Mat(_size, CV_8UC3);
    }
    else
    {
        std::cout<<" AVessel :: camera is not available --> terminating program!"<<std::endl;
        exit(-1);
    }


    /// Calibrator
    calibrator.reset( new MonoCalib(_size) );

    // Load calibration files
    if(config.filename_intrinsic.size() > 0)
        calibrator->loadConfiguration_INTRINSIC(config.filename_intrinsic);
    if(config.filename_intrinsic.size() > 0)
        calibrator->loadConfiguration_EXTRINSIC(config.filename_extrinsic);

    switch (config.mode )
    {
    case AVessel_INTRINSIC:
        calibrator->setCalibrationType(_CalibINTRINSIC);
        break;
    case AVessel_EXTRINSIC:
        calibrator->setCalibrationType(_CalibEXTRINSIC);
        break;
    default:
        calibrator->setCalibrationType(_CalibOFF);
    }




    /// TIMER CICLE
    timer_cicle = new QTimer();
    timer_cicle->setInterval((int) (1.0 / config.fps * 1000));
    QObject::connect(timer_cicle, SIGNAL(timeout()), this, SLOT(slot_mainCicle()));
    timer_cicle->start();

}


AVessel::~AVessel()
{
    videoCapture->release();
}


/**
 * @brief AVessel::loadConfigurationProgram
 * @param filename is the directory where the file exists
 * @param pconfig is the structure for output
 * @return true if success
 *
 * @note Example of a file:
 * %YAML:1.0
 * IDCAM: 0
 * MODE: 1
 * DIRECTORY_INTRINSIC: /media/andrypinto/DATA/
 * DIRECTORY_EXTRINSIC: /media/andrypinto/DATA/
 * CALIB_BOARD_WIDTH: 9
 * CALIB_BOARD_HEIGH: 6
 * CALIB_BOARD_SIZE: 0.025
 * FPS: 7.0
 * SCALEImage: 1.0
 * SEG_COLOR_MEAN: 90. 210. 80. 0.
 * SEG_COLOR_STD: 50. 30. 50. 0.
 */
bool AVessel::loadConfigurationProgram(const std::string &filename, _configProgram & pconfig)
{
    cv::FileStorage fs;
    if (fs.open(filename, cv::FileStorage::READ))
    {
        fs["IDCAM"]                  >> pconfig.idCam;
        int val                      = 2;
        fs["MODE"]                   >> val;
        pconfig.mode                 = (_ModeAVessel) val;
        fs["CALIB_NUMB_IMAGES"]      >> pconfig.calib_numb_frames;
        fs["DIRECTORY_INTRINSIC"]    >> pconfig.filename_intrinsic;
        fs["DIRECTORY_EXTRINSIC"]    >> pconfig.filename_extrinsic;
        fs["CALIB_BOARD_WIDTH"]      >> pconfig.calib_board_size.width;
        fs["CALIB_BOARD_HEIGH"]      >> pconfig.calib_board_size.height;
        fs["CALIB_BOARD_SIZE"]       >> pconfig.calib_board_distance;
        fs["FPS"]                    >> pconfig.fps;
        fs["SCALEImage"]             >> pconfig.scaleImage;
        fs["SEG_COLOR_MEAN"]         >> pconfig.seg_colorMean;
        fs["SEG_COLOR_STD"]          >> pconfig.seg_colorStd;

        std::cout<<" File loaded:"<<filename.c_str()<<std::endl;
        return true;
    }

    return false;
}



void AVessel::slot_mainCicle(void)
{
    /// ACQUISITION IMAGE.
    videoCapture->read(img);


    arr_centroids.clear();

    /// *****************************************************
    /// Undistort image - rectifies if possible (if calibration matrices exist).
    cv::Mat img_undistorted(img.rows, img.cols, img.type(), cv::Scalar::all(0));
    if (calibrator->undistort(img, img_undistorted))
    {
        /// SEGMENTATION
        cv::Mat seg_dest;
        segmentation_Color(img, seg_dest);
        //        cv::imshow("Seg", seg_dest);
        extractCentroid(seg_dest, arr_centroids);


        /// Convert pixel to World Frame.
        arr_centroids_World.clear();
        for( int i = 0; i < arr_centroids.size(); i++)
        {
            cv::Point3d wordPoint;
            UTILS::pixel2world(arr_centroids[i], wordPoint, calibrator->K_cameraMatrix, calibrator->Ext_Rot, calibrator->Ext_Trans, calibrator->Ext_Trans.at<double>(0,2));
            arr_centroids_World.push_back(wordPoint);

            /// TODO ---> [Send this information]
        }
    }
    /// *****************************************************





    /// CALIBRATION PROCESS.
    if(_step > 60)
    {
        std::vector<cv::Point2f> chessboard_imageCorners; //used due to laser calibration
        bool flag_calibration = calibrator->process_calibration_camera(img, config.calib_board_size, config.calib_board_distance, config.calib_numb_frames, chessboard_imageCorners);
        _step = 0;
    }



    if(img_undistorted.size() != cv::Size(0,0))
        cv::imshow("Live", img_undistorted);

    _step++; //debug for offline processing.
    return;
}







/*!
 * \brief AVessel::segmentation_Color
 * \note the segmentation is conducted in YUV space - using the UV set
 * \note internal variables required: "laser_colorMean" and "laser_colorStd"
 *
 *
 * \param src is CV_8UC1 or CV_8UC3 in BGR
 * \param dst is CV_8UC1 which represents the segmentation mask
 *
 * @note BGR to yCrCb:
 * @note  (0, 0, 255) ---> (76, 255, 85)
 * @note  (0, 255, 0) ---> (150, 21, 43)
 * @note  (255, 0, 0) ---> (29, 107, 255)
 */
void AVessel::segmentation_Color(const cv::Mat &src, cv::Mat &dst)
{
    dst = cv::Mat::zeros(src.rows, src.cols, CV_8UC1);
    cv::Mat uyv_src;
    cv::cvtColor(src, uyv_src, CV_BGR2YUV);
    std::vector<cv::Mat> arr_src;      //split image
    cv::split(uyv_src, arr_src);

    cv::Mat dst_u, dst_v;
    segmentation_Channel(arr_src[1], dst_u, (int) (config.seg_colorMean.val[1] - config.seg_colorStd.val[1]), (int)(config.seg_colorMean.val[1] + config.seg_colorStd.val[1]));
    segmentation_Channel(arr_src[2], dst_v, (int) (config.seg_colorMean.val[2] - config.seg_colorStd.val[2]), (int)(config.seg_colorMean.val[2] + config.seg_colorStd.val[2]));

    //    cv::namedWindow("Y");
    //    cv::namedWindow("U");
    //    cv::namedWindow("V");
    //    cv::imshow("Y", arr_src[0]);

    //    cv::imshow("U", arr_src[1]);

    //    cv::imshow("V", arr_src[2]);

    dst = dst_u & dst_v;
    return;
}



/*!
 * \brief AVessel::segmentation_Channel
 * \param src is CV_8UC1
 * \param dst is CV_8UC1 which represents the segmentation mask
 */
void AVessel::segmentation_Channel(cv::Mat &src, cv::Mat &dst, int thr_min, int thr_max)
{
    dst = cv::Mat::zeros(src.rows, src.cols, CV_8UC1);

    for(int irow = 0; irow < src.rows; irow++)
    {
        uchar * pixel      = src.ptr<uchar>(irow);
        uchar * pixel_mask = dst.ptr<uchar>(irow);

        for(int icol = 0; icol < src.cols; icol++)
        {
            if((pixel[icol] > thr_min) && (pixel[icol] < thr_max))
                pixel_mask[icol] = 255;
        }
    }
    return;
}



/**
 * @brief AVessel::extractCentroid -  extract the contours and centroids from the "src_seg"
 * @param src_seg -  CV_8UC1 (segmented image)
 * @param arr_centroids is an array of centroids capturing the contours.
 */
void AVessel::extractCentroid(const cv::Mat & src_seg, std::vector<cv::Point2d> &arr_centroids )
{

    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;

    /// Find contours
    cv::findContours( src_seg, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );

    /// Get the moments
    std::vector<cv::Moments> mu(contours.size() );
    for( int i = 0; i < contours.size(); i++ )
    {
        mu[i] = cv::moments( contours[i], false );
    }

    ///  Get the mass centers:
    arr_centroids.clear();
    //std::vector<int> contour_idx;
    for( int i = 0; i < contours.size(); i++ )
    {
        if(mu[i].m00 < 10)
            continue;
        arr_centroids.push_back(cv::Point2d( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 ));
        //contour_idx.push_back(i);
    }
    return;

    //    /// Draw contours
    //    cv::Mat drawing = cv::Mat::zeros( src_seg.size(), CV_8UC3 );
    //    for( int i = 0; i< arr_centroids.size(); i++ )
    //    {
    //        cv::Scalar color = cv::Scalar( 255, 0, 255 );
    //        cv::circle( drawing, arr_centroids[i], 4, color, -1, 8, 0 );
    //    }

    //    /// Show in a window
    //    cv::namedWindow( "Contours", CV_WINDOW_AUTOSIZE );
    //    cv::imshow( "Contours", drawing );
    return;
}

